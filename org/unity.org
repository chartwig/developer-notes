* Remembrall
** This vs. That
*** Instantiate with parent transform vs. Instantiate from a transform
 - If you use `Instantiate(object, parentTransform)`, note that all
   instantiated objects have transforms relative to that `parentTransform`.
   So, if the `parentTransform` moves, so do all of the child transforms.
 - If you use `Instantiate(object, someTransform.position, rotation)`, note
   that all instantiated objects have independent transforms, and are not
   relative to `someTransform.position`. If you move `someTransform`, they
   will not move with it.
*** Rigidbody.velocity vs. updating the transform.position
 - If you add a Rigidbody component to the GameObject, and then set a
   its velocity, you can cause your object to move while respecting physical
   forces like gravity or drag, using Unity's built in physics engine.
 - If you update the `transform.position` instead, it will simply move linearly
   in whatever direction you choose. It will not respect any physical forces.
** How To's
*** How to stop Friendly Fire and other unwanted collisions
You can use layers to group like things together, and
then use the Physics and Physics 2D settings to control
how layers collide against each other. Don't want friendly
fire? Add PC's to a layer, let's say "A", and their attacks
into anotehr, let's say "B". Then you go to the collision
matrix in the Physics and Physics2D settings and turn off
collisions between layers A and B. 
*** Do something repeatedly while an Input is held down
 - Create your action. Let's say it is something like `transform.position.x += 1`
 - Put that into a routine, yo
 - Name that routine...howabout `MoveRight` (Yes, I know there are better 
   ways to move)
 - Now, you use InvokeRepeating to repeatedly call your awesome new `MoveRight`
   routine
 - Put that into other actions, like checking input for a spacebar or something:
   ```
   if(Input.GetKeyDown(KeyCode.Space){
       InvokeRepeating("MoveRight", 0.000001f, 1);
   }
   ```   
 - Now, if they press space, the gameObject will MoveRight every 1 second from
   a start of crazy weird value
 - NOTE: That crazy weird value of 0.000001f is because if you simply put 0.0f,
   it might do multiple invocations immediately, rather than once every second
   after essentially 0.0f. 
 - Make sure you have a `CancelInvoke("MoveRight")` somewhere to keep your 
   awesome routine in check.
*** Ensure that moving objects move smoothly
 - When updating the velocity of an object, or updating its
   `transform.position` each frame, make sure that you 
   multiply the velocity, or the direction and speed if you
   update the transform and not the velocity, against
   `Time.deltaTime`. This will ensure that your object moves
   at a rate that is independent of the frame rate, rather
   than gittering due to a changing frame rate.
*** Translate the Viewport (Screen Edge) coordinates to World Points
 - Use Camera.ViewportToWorldPoint
 - Set the x and y values of ViewportToWorldPoint to be from 0 to 1,
   based upon which edge you want to measure to. 0 is left and bottom,
   1 is right and top
 - Make sure that you set the z value of ViewportToWorldPoint to be
   the distance of the camera from the object - this ensures that it
   can measure where the Viewport edges are in relation to the
   camera and the object
*** Split Tilesheets
 - First, add the Tilesheet as an asset.
 - Second, set it to multiple, and set the pixels per unit
 - Third, Open the Sprite Editor, and split the sprite into multiple sprites
 - Fourth, add the Tile Palette window
 - Fifth, drag the tilesheet to the Tile Palette
 - Sixth, save the new Tile Pallete and sprites
 - Seventh, bask in the awesome glow of all of your individual sprites
** Gotcha's
*** Destroy GameObjects when no longer needed
 - Let's say you have a spaceship firing lasers. You invoke hundreds of them
   by firing your lasers, but you never destroy them. Eventually, your game
   gets slower and slower.
 - Never fear! Remember to call `Destroy(gameObject)` on your laser prefabs.
*** SO MANY INVOCATIONS
 - Lemme guess, you called `InvokeRepeating`, right?
 - Did you call `CancelInvoke`?
 - No?
 - Do that, when you no longer need to continue invoking
*** Multiple Invocations at 0.0f when I call `InvokeRepeating`
 - Do: Call `InvokeRepeating`, passing a value slightly larger than 0, like 0.000001f.
 - Don't: Call `InvokeRepeating`, passing 0.0f.
*** Where are my LINQ FUNCTIONS??
Trying to use a Linq function with Transform or some other IEnumerable?
The problem is that you need IEnumerable<Transform>, so you need to call
Cast<Transform> on your Transform first.
*** Setting the parent transform but not changing your offsets
 - NOTE: this is if you handle all of it programmatically
 - If you set the parent transfer alone, it will set your new position to be an
   absolute position set by the parent transform + your offsets. So, if your
   parent transform is at like (30, 30, 0), and your offset is (15, 15, 0),
   then your new position is (45, 45, 0), and will move with the parent while
   maintaining the offset
 - If you set the offsetMin and offsetMax appropriately, say (0, 0, 0), then you
   can have a prefab rest at the same position as the parent. This is more
   important for UI elements, where trying to get a UI element to rest inside a
   parent, rather than be off by 500 or something crazy.

More info:
https://stackoverflow.com/questions/42533697/simplify-foreach-on-transform
** Performance!!
*** Use Layers to reduce Collisions
Remember how we controlled Friendly Fire? Well, make sure you
you only have objects collide *that actually need to*, and you
can reduce the amount of code that you execute to only what
ever needs to execute.
*** How to properly compare Tags
If you use tags, always use CompareTag(tag) instead of (object.tag == tag) - the latter has performance problems.
